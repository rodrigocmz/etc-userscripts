// ==UserScript==
// @name        TSM
// @namespace   etc-userscripts
// @match       *://*:38001/*
// @grant       none
// @version     2.0.2
// @downloadURL https://bitbucket.org/rodrigocmz/etc-userscripts/raw/master/src/tsm.user.js
// @author      Rodrigo Martinez (https://www.linkedin.com/in/rodrigo-carvalho-martinez/)
// @description TSM — for learning purposes only!
// @license     CC-BY-SA-4.0; https://creativecommons.org/licenses/by-sa/4.0/
// ==/UserScript==
let tsmStart = window.performance.now();
const tsmLogPrefix = '[TSM] ';

// ALTERE ESTA VARIÁVEL PARA INSERIR AS MARCAÇÕES
const tsmHorarios = ['10:00', '13:00', '14:00', '19:00'];
const posEntrada = 0; // Posição/índice da entrada. Sempre 0.
const posSaida = tsmHorarios.length - 1; // Posição/índice da saida. Sempre a última (length - 1).
const posAlmoco = 1; // Posição/índice do almoço. Sempre 1 se não houver algum intervalo entre a entrada e almoço.

// Constantes de variação no horário. Atribua = 0 para variação nenhuma.
const tsmVariacaoMinutosAntecipacao = 30;
const tsmVariacaoMinutosAtraso = 15;
const tsmVariacaoMinutosAlmocoExtendido = 40;
const tsmIntervaloMinutos = 5; // Arredonda de 5 em 5 minutos

var tsmNovosHorarios = Array.from(tsmHorarios); // Array onde serão aplicadas as variações


function tsmLogElapsedTime() {
    console.info(tsmLogPrefix + 'Execution time: ' + (window.performance.now() - tsmStart).toFixed(2) + ' ms');
}

// Based on https://bobbyhadz.com/blog/javascript-round-number-to-nearest-five
function randomizeMinutes(limit) {
    let num = Math.floor(Math.random() * (limit + 1));
    return Math.round(num / tsmIntervaloMinutos) * tsmIntervaloMinutos;
}

/**
 * Antecipa a entrada e a saída com a variação da tsmVariacaoMinutosAntecipacao.
 *
 */
function tsmAntecipaEntrada() {
    let variacao = randomizeMinutes(tsmVariacaoMinutosAntecipacao);
    let d = new Date();
    d.setHours(tsmNovosHorarios[posEntrada].split(":")[0]);
    d.setMinutes(tsmNovosHorarios[posEntrada].split(":")[1]);
    d.setTime(d.getTime() - (variacao * 60 * 1000));
    tsmNovosHorarios[posEntrada] = String(d.getHours()).padStart(2, '0') + ":" + String(d.getMinutes()).padStart(2, '0');
    d = new Date();
    d.setHours(tsmNovosHorarios[posSaida].split(":")[0]);
    d.setMinutes(tsmNovosHorarios[posSaida].split(":")[1]);
    d.setTime(d.getTime() - (variacao * 60 * 1000));
    tsmNovosHorarios[posSaida] = String(d.getHours()).padStart(2, '0') + ":" + String(d.getMinutes()).padStart(2, '0');
}

/**
 * Atrasa a entrada e a saída com a variação da tsmVariacaoMinutosAtraso.
 *
 */
function tsmAtrasaEntrada() {
    let variacao = randomizeMinutes(tsmVariacaoMinutosAtraso);
    let d = new Date();
    d.setHours(tsmNovosHorarios[posEntrada].split(":")[0]);
    d.setMinutes(tsmNovosHorarios[posEntrada].split(":")[1]);
    d.setTime(d.getTime() + (variacao * 60 * 1000));
    tsmNovosHorarios[posEntrada] = String(d.getHours()).padStart(2, '0') + ":" + String(d.getMinutes()).padStart(2, '0');
    d = new Date();
    d.setHours(tsmNovosHorarios[posSaida].split(":")[0]);
    d.setMinutes(tsmNovosHorarios[posSaida].split(":")[1]);
    d.setTime(d.getTime() + (variacao * 60 * 1000));
    tsmNovosHorarios[posSaida] = String(d.getHours()).padStart(2, '0') + ":" + String(d.getMinutes()).padStart(2, '0');
}

/**
 * Altera a volta do almoço dependendo da tsmVariacaoMinutosAlmocoExtendido.
 * Altera também o horário de saída com a mesma variação.
 */
function tsmExtendeAlmoco() {
    let variacao = randomizeMinutes(tsmVariacaoMinutosAlmocoExtendido);
    let d = new Date();
    d.setHours(tsmNovosHorarios[posAlmoco + 1].split(":")[0]);
    d.setMinutes(tsmNovosHorarios[posAlmoco + 1].split(":")[1]);
    d.setTime(d.getTime() + (variacao * 60 * 1000));
    tsmNovosHorarios[posAlmoco + 1] = String(d.getHours()).padStart(2, '0') + ":" + String(d.getMinutes()).padStart(2, '0');
    d = new Date();
    d.setHours(tsmNovosHorarios[posSaida].split(":")[0]);
    d.setMinutes(tsmNovosHorarios[posSaida].split(":")[1]);
    d.setTime(d.getTime() + (variacao * 60 * 1000));
    tsmNovosHorarios[posSaida] = String(d.getHours()).padStart(2, '0') + ":" + String(d.getMinutes()).padStart(2, '0');
}

function tsmAplicaVariacao() {
    if (tsmVariacaoMinutosAntecipacao > 0 && tsmVariacaoMinutosAtraso > 0) {
        if (Math.floor(Math.random() * 2) == 0) { // "par ou ímpar"
            tsmAntecipaEntrada();
        } else {
            tsmAtrasaEntrada();
        }
    } else if (tsmVariacaoMinutosAntecipacao > 0) {
        tsmAntecipaEntrada();
    } else if (tsmVariacaoMinutosAtraso > 0) {
        tsmAtrasaEntrada();
    }

    if (tsmVariacaoMinutosAlmocoExtendido > 0) {
        tsmExtendeAlmoco();
    }

}

// O código executado assim que a página carrega está lá no final do arquivo.

/**
 * Função ativada pelo clique no botão injetado na tela.
 * @see tsmFillData
 */
function tsmClick() {
    let tsmMestre = window.frames["MESTRE"];
    // Monte de IF só pra tratar o erro no console caso o sistema tenha mudado o layout da página
    let found = false;
    if (tsmMestre) {
        let tsmDadosAcerto = tsmMestre.frames["DADOSACERTO"];
        if (tsmDadosAcerto) {
            let tsmDados = tsmDadosAcerto.frames["DADOS"];
            if (tsmDados) {
                tsmNovosHorarios = Array.from(tsmHorarios); // Zera tudo
                tsmAplicaVariacao();
                tsmFillData(tsmDados);
                found = true;

            }
        }
    }
    if (!found) {
        console.warn(tsmLogPrefix + 'Hierarquia de frames MESTRE.DADOSACERTO.DADOS não encontrada na página.');
    }
}

/*
 * Preenche o form com o número de linhas definio pela variável tsmHorarios.
 * @param tsmDados Frame "DADOS" da página de acertos individuais.
 */
function tsmFillData(tsmDados) {
    let tsmDia = tsmDados.document.getElementsByName("DatAce")[0];
    if (tsmDia) {
        for (let i = 0; i < tsmNovosHorarios.length; i++) {
            let tsmFieldIndex = String(i + 1).padStart(2, '0')
            tsmFieldFill(tsmDados.document.getElementsByName("DDM" + tsmFieldIndex)[0], tsmDia.value);
            tsmFieldFill(tsmDados.document.getElementsByName("DHM" + tsmFieldIndex)[0], tsmNovosHorarios[i]);
            tsmFieldFill(tsmDados.document.getElementsByName("DJU" + tsmFieldIndex)[0], '2');
        }
    } else {
        console.warn(tsmLogPrefix + 'Campo hidden "DatAce" do frame "DADOS" não encontrado na tela.');
    }
}

/*
 * Preenche o campo @field com o valor @value.
 * A função dispara os eventos focus() e blur() para o sistema processar, mitigando o risco de automações nativas dele quebrarem.
 * @param field Objeto HTML a ser prennchido.
 * @param @value valor a ser preenchido no campo.
 */
function tsmFieldFill(field, value) {
    if (field) {
        field.focus();
        field.value = value;
        field.blur();
    }
}

// TRECHO EXECUTADO ASSIM QUE A PÁGINA É CARREGADA:

/*
 * Aguarda 1 segundo porque o Rodapé é criado e DEPOIS preenchido pelo sistema automaticamente.
 * Esse 1 segundo garante que quando tentarmos achar o bloco pra inserir o botão, o bloco já existirá.
 */
setTimeout(function () {
    let tsmRodape = window.frames["RODAPE"];
    if (tsmRodape) {
        let tsmRodapeTR = tsmRodape.document.querySelector("body > table > tbody > tr");
        if (tsmRodapeTR) {
            var node = tsmRodape.document.createElement("td");
            node.innerHTML = '<td><input type="button" id="tsm-button" class="BOTAO" accesskey="u" value="✎ v2.0.2"/></td>';
            tsmRodapeTR.appendChild(node);
            let tsmButton = tsmRodape.document.getElementById("tsm-button");
            tsmButton.addEventListener("click", tsmClick);
        }
    }
}, 1000);

/*
 * RELEASE NOTES:
 * 2.0.2: Inclusão de lógica pra arredondar de X em X minutos (5 default) e padronização de log.
 * 2.0.0: Inclusão de variação pseudo-aleatória de horários.
 * 1.0.0: Criação.
 */
