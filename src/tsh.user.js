// ==UserScript==
// @name        TSH
// @namespace   Study Userscripts
// @match       https://*:47601/*
// @grant       none
// @version     1.0.1
// @author      🧑‍🎓
// @description Userscript for studying and tinkering
// ==/UserScript==

// Possible entries
const tshMyPossibleTimes = [
  ['10:00', '12:00', '13:00', '19:00'], ['10:00', '13:00', '14:00', '19:00'], ['10:00', '12:00', '14:00', '20:00'],
  ['09:00', '12:00', '14:00', '19:00'], ['09:30', '12:00', '14:00', '19:30'],
  ['10:30', '12:00', '13:00', '19:30'], ['10:30', '13:00', '14:00', '19:30'], ['10:30', '12:00', '13:30', '20:00'], ['10:30', '13:00', '14:30', '20:00']
];

const tshRandomizePossibleTimes = false; // Or else it goes sequentially

logI('Initialized');
window.TSH = {};
window.TSH.position = 0;
window.TSH.interval = setInterval(function() {
  if (window.top != window.self && window.name == 'ci') {
    let addButton = document.querySelector("#addMarcacao");
    if (addButton) {
      let tshDiv = document.querySelector("#tshDiv");
      if (!tshDiv) {
        tshDiv = document.createElement('div');
        tshDiv.className = 'btn-group margin-left-5';
        tshDiv.id = 'tshDiv';
        tshDiv.innerHTML = '<button type="button" class="btn" id="tshButton">✍️ Helper</button>';
        addButton.parentNode.appendChild(tshDiv);
        document.querySelector("#tshButton").addEventListener("click", tshFill);
        window.TSH.position = 0;
        logI('Widget rendered on screen');
      }
    }
  }
}, 1000);

function tshFill() {
  let timeEntry = document.querySelector("#marcacaoTime-3");
  if (tshMyPossibleTimes.length > 1) {
    if (tshRandomizePossibleTimes) {
      window.TSH.position = Math.floor(Math.random() * (tshMyPossibleTimes.length));
    }
    else if (timeEntry) {
      if (window.TSH.position < tshMyPossibleTimes.length - 1) {
        window.TSH.position++;
      }
      else {
        window.TSH.position = 0;
      }
    }
  }
  let timeArray = tshMyPossibleTimes[window.TSH.position];
  let addButton = document.querySelector("#addMarcacao");
  for (let i = 0; i < 4; i++) {
    if (!timeEntry) {
      addButton.click();
    }
    let tshEntry = document.querySelector("#marcacaoTime-" + i);
    tshEntry.value = timeArray[i];
    tshEntry.dispatchEvent(new Event("change"));
  }
  logI('Slots pre-filled with tshMyPossibleTimes[' + window.TSH.position + ']');
}

function logI(text) {
  console.info('[Userscript] [TSH] ' + text);
}
