# README #

Userscripts for testing, learning and sharing.
Unless otherwise noted in the script, it's licensed under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/)

Downloads should be made through the "raw" path.
For example:
https://bitbucket.org/rodrigocmz/etc-userscripts/raw/master/src/tsm.user.js